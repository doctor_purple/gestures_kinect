import processing.core.*;
import SimpleOpenNI.*;
//Modifica
//Mergiami sto cazzooooooo
//Mergiami sto cazzooooooo
//Modifica 2
public class SkeletonRecog extends PApplet{
	
	//defaul serial ID
	private static final long serialVersionUID = 1L;
	
	
	SimpleOpenNI context;
	//Real world coords
	PVector head;
	PVector leftHand;
	PVector rightHand;
	PVector usrPos;
	//Projective coords
	PVector vectProj;
	PVector leftHandProj;
	PVector rightHandProj;
	PVector usrPosProj;
	
	int jointWidth;
	public void setup(){
		leftHand = new PVector();
		rightHand = new PVector();
		head = new PVector();
		usrPos = new PVector();
		
		vectProj = new PVector();
		
		context = new SimpleOpenNI(this);
		context.enableDepth();
		context.setMirror(true); //setta il mirror a true
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		
		background(200,0,0);
		
		ellipseMode(CENTER);
		
		smooth();
		size(context.depthWidth(), context.depthHeight());
		println("Larghezza "+context.depthWidth()+" Altezza "+context.depthHeight());
		
	}
	public void draw(){
		context.update();
		image(context.depthImage(),0,0);
		
		//COntrollo se c'� lo scheletro di qualche utente e lo disegno
		int i;
		for(i=1;i<=10;i++){
			if(context.isTrackingSkeleton(i)){
				drawSkeleton(i);
			}
			
		}
	}

	public void drawSkeleton(int userId){
		stroke(0,0,255);
		strokeWeight(10);
		jointWidth = 10;
		context.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
		
		context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
		context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
		context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
		
		context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
		context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
		context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
		
		context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
		context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
		
		context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
		context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
		context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
		
		context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
		context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
		context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
		
		stroke(255,0,0);
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_HEAD, head);
		context.convertRealWorldToProjective(head,vectProj); 
		ellipse(vectProj.x,vectProj.y,jointWidth*4,jointWidth*4);
		println("TESTA-------------- X: "+head.x+" X proj: "+vectProj.x+"||| Y: "+head.y+" Y proj:"+vectProj.y);
		
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_PROFILE_ALL, usrPos);
		context.convertRealWorldToProjective(usrPos,vectProj);
		ellipse(vectProj.x,vectProj.y,jointWidth*2,jointWidth*2);
		
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HAND, leftHand);
		stroke(0,255,0);
		context.convertRealWorldToProjective(leftHand,vectProj);
		ellipse(vectProj.x,vectProj.y,jointWidth,jointWidth);
		
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HAND, rightHand);
		context.convertRealWorldToProjective(rightHand,vectProj);
		ellipse(vectProj.x,vectProj.y,jointWidth,jointWidth);
		
		//AGGIUNTO DA MARCO per la seconda volta
		text("Daniele è un figo pazzesco", 200, 100);
	}
	
	//Eventi scatenati dal riconoscimento dell'utente
	public void onNewUser(int userId){
		System.out.println("Entrato utente "+userId);
		context.startPoseDetection("Psi", userId); //L'inizio del riconoscimento posa lo monitoro con onStartPose
	}
	
	public void onLostUser(int userId){
		System.out.println("Utente "+userId+" perso");
	}
	
	public void onStartPose(String pose,int userId){
		System.out.println("Inizio riconoscimento posa "+pose+" dell'utente "+userId);
		//Ora che hai trovato una posa smetti di cercarla per questo utente
		context.stopPoseDetection(userId);
		//Inizia a calibrare lo scheletro
		context.requestCalibrationSkeleton(userId, true);
	}
	
	//Funzioni chiamate quando la calibrazione dello scheletro inizia e finisce
	public void onStartCalibration(int userId){
		println("Inizio calibrazione utente"+userId);
	}
	
	public void onEndCalibration(int userId, boolean successfull){
		println("Calibrazione utente "+userId+" terminata con successo? "+successfull);
		
		if(successfull){
			println("Utente calibrato :)");
			context.startTrackingSkeleton(userId);
		}else{
			println("Non sono riuscito a calibrare l'utente :(");
			//Quindi riprovo con il riconoscimento della posa
			//context.startPoseDetection("Psi",userId);
			context.requestCalibrationSkeleton(userId, false);
			
		}
	}
	
}
